## Update Changelog - 2021-01-09 (Ep. 10.3 Pre-trans War of Emperium tryout event)

### Changelog for Players

#### Added
- **WoE**: Added Pretrans-WoE tryout event:
  - One pre-trans Castle will be open every Saturday from 19:00 UTC till 20:00 UTC.
  - The Castle will only be manually and irregularly rotated for now.
  - Disabled trans items. [Click her for the up to date blacklisted items](https://gitlab.com/originsro/originsro/-/blob/master/changelogs/attachments/pre-trans-woe-item-blacklist.md) [8682986dcf, 4775c0bbde, b45d536950, 9b0024fd14, a4de682942, f8ef9cddaf]
  - Disabled Ninja skills learned through Plagiarism. [62fd8584c3, a751248165, d4f1a3248e]
  - Automatically removed trans buffs [9da92a6282, 84d0eadee9]
  - Disabled 2nd Class Quest Skills [09fe9c6d35, d74711e6cc]
  - Automatically removed spirit spheres when a non-monk-type class enters a pre-trans map. [df8981526b]
  - Modified the Emperium stats to their kRO variant during pretrans WoE. [51bce89cf0]
  - Excluded pretrans castles from the Town Flags for now. [fa1ecf06da]

#### Changed

- **Mobs**: Removed Constant from the Mob of the Day list. [f0a727065d]
- **Commands**: Improved the display format of `@mobinfo` / `@mi`. [c3879b57c2]
- **Misc**: Performance improvements. [321bb9b2c0]
- **WoE**: Made the spawned WoE castle treasure chests persist across a server reboot. [a33b809a1a]
- **Event**: Changed the color of event warp portals to yellow to differentiate them from normal warp portals. [927eb01be8]
- **Skills**: Disabled use of the Teleport skill while storage open, as it would cause a client crash otherwise. Related to originsro/originsro#2234 [1f8cbed04c]
- **Skills**: Implemented official behavior of Super Novice Spirit: [9481880ced, 242493e062, b318bb16e2]
  - You can equip any Headgear when base level 90 or higher.
  - You can equip the following level 4 weapons when base level 96 or higher: Daggers, 1H-Swords, 1H-Axes, 2H-Axes, Maces, Staffs, 2H-Staffs.
- **Skills**: Changed Super Novice Spirit to *only* unequip spirit-exclusive items when dying. Related to originsro/originsro#1822 [9367deb299]
- **Skills**: Changed Super Novice Spirit to *not* unequip spirit-eclusive items when escaping death by Guardian Angel. [da51c7c579]
- **Skills**, **UI**: Updated the description of Super Novice Spirit to match official behavior. [24896db621]
- **UI**: Excluded Skin folders from reporting errors when files are missing, as some skins do this intentionally.
- **Client**: Improved responsiveness when using multiple items in a row.
- **CP**: Changed some "Dark" element leftovers to "Shadow". [02f897675b]

#### Fixed

- **Mobs**: Fixed some issues with the tombstone-dodging function for MVP dropped items. [97ef24da41]
- **NPCs**: Fixed an issue that caused the level 99 aura to appear on some NPCs. [eafd9582b4]
- **Misc**: Fixed a position glitch in the chat message balloons of a hidden character. [9cffd9e07a]
- **Misc**: Fixed messages from Frost Joker, Dazzler and pets appearing inside chat room. [848d480ff2]
- **Commands**: Fixed an issue that prevented autotrade from automatically expiring after 7 days for characters that were autotrading when the server is restarted. [763fc68602, 87717fe288, 65d737a843]
- **UI**: Fixed some issues in the MVP damage log system. [9a4e12094b]
- **WoE**: Fixed an issue that could allow a Gunslinger or a Ninja to stay in a pre-trans WoE castle if they were already in at the beginning of WoE. [af33505707]
- **WoE**: Improved the castle rotation system for better compatibility with pre-trans WoE and multiple WoE sessions on the same day. [05f529d000, baafcc6ac5, fc099c68b7, c38e59375d]
- **Skills**: Fixed Shield skills being affected by masteries. Related to originsro/originsro#1610 [2d3ff4e706]
- **Skills**: Fixed Shield Boomerang being affected by the 20% damage bonus of Magnum Break. Related to originsro/originsro#1610 [09c19ee8d6]
- **Skills**: Fixed Shield Boomerang being affected by Spirit Spheres. Related to originsro/originsro#1610 [474af1e60c]
- **Skills**: Fixed Shield Chain not giving a flat 20% hit bonus. originsro/originsro#1671 [65b484c154]
- **UI**: Fixed the guild emblem not getting updated when a character joins or leaves a guild. [0544b8568b]
- **Skills**: Fixed the duration of the Super Novices' Steel Body buff when dying at 99% exp. Related to originsro/originsro#1898 [84c7e5b68d]
- **Skills**: Fixed the cast time and after cast delay of Napalm Beat. Related to originsro/originsro#1940 [41c62018c5]
- **Skills**: Fixed Undead race monsters getting frozen or stone-cursed. Related to originsro/originsro#1111 [4601e419ba]
- **Skills**: Fixed Shield Boomerang and Shield Chain not using the ATK derived from base stats only. This prevents Provoke and Auto Berserk from amplifying the damage as well. Related to originsro/originsro#1663 [12f6a00f78, 711e107b46, ebd67d53b2]
- **Skills**: Fixed units that cannot be damaged (i.e. Fire Pillar) from causing Sight Blaster to be stuck dealing damage till they time out. [95e3d8ca72]
- **Skills**: Fixed Wink Charm spamming its emoticon too fast. [c95003e0a6]
- **Skills**: Made Costume Headgears equipable when under the effect of Super Novice Spirit. Related to originsro/originsro#2051, originsro/originsro#2358 [c8b5dd4501]
- **Items**: Fixed Lightning Sphere Pack not giving any spheres. [27296e789b]
- **UI**: Fixed Guild Flags not updating their Emblem when the owner changes.
- **UI**: Fixed the monster rightclick feature accidentally opening for Pets.
- **UI**: Fixed the width calculation for the "Item obtained" notification rectangle.

> This update consists of 106 commits
