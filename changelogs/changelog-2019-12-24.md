# Update Changelog - 2019-12-24 (part of Ep. 10.1 patch 1)

#### Added

- **Events**: Added content related to the Christmas 2019 event. See the Lunatic in Lutie Field to get started. Note: the rewards can be claimed only once per Master Account.
- **Commands**: Removed the healing visual effect when the client has simplified effects enabled (i.e. `/mineffect` or WoE) in order to improve the game frame rate. The `/showheal` command has been provided, to temporarily re-enable the visual effect for those that wish to see it in those environments.
- **Replay**: Added x8 and x16 playback speed in the Replay Theater mode.

#### Changed

- **Items**: Re-enabled the Assumptio autospell in the Super Novice set.
- **Items**: Standardized the spelling of Byeol-ungeom.
- **Commands**: Improved the Zeny formatting in `@whosells` and `@whobuys`.
- **Quests**: Kiel Hyre will now delete any leftover quest items (such as the keys) when talking to him again after the quest is completed.
- **Skills**: Re-added other Alchemist skills to the idle timer reset exceptions, to prevent abuse:
  - Pharmacy
  - Call Homunculus
  - Rest
  - Homunculus Resurrection
- **NPCs**: Moved the Juno Pub Master one cell farther from the Inn Employee.
- **Skills**: Changed the Mom, Dad, I Love You! skill to no longer require the baby and its parents to be in the same map. This is a custom QoL improvement, allowing a multiclienting parent to take full advantage of the OriginsRO party bonus while keeping their child safe in town, with the intent to improve the birth and adoption rates throughout the kingdom of Rune-Midgarts.
- **NPCs**: Updated the Valkyrie NPC to honor the "Allow skill and status point allocation" guest setting. Guests that don't have that flag enabled will be unable to rebirth or re-rebirth. originsro/originsro#1633
- **Maps**: Tweaked the location of some warp portals and opened the access to a building in Juno.
- **Mobs**: Renamed Demon Pungus to the officially recognized correct spelling, Demon Fungus.
- **Mobs**: Changed the respawn time of some fixed spawns (Aldebaran Dungeon B3, Fortress St. Darmain 6 and 8) to prevent abuse. Monsters with a small respawn area now have a respawn time of no less than 30~60 seconds, depending on the area size. originsro/originsro#832
- **Misc**: Performance improvements.
- **Chat**: The `#support` channel is now automatically joined on login, to improve its visibility and usefulness. originsro/originsro#1537
- **Misc**: Improvements to the cheat detection system.
- **Commands**: Improved formatting in commands that display the time.
- **Commands**: Disabled reporting of the amount of players in a map through `@cl` in GvG environments.
- **Chat**: Added a cooldown to the `#party` channel. The cooldown is 60 seconds per party and two minutes per master account. Attempts to circumvent the rate limit by using alternate accounts or characters will be subject to mutes by GMs. originsro/originsro#772
- **Items**: Improved the "Ammunition has been equipped" message to include the item name.
- **Skills**: Added a 10 second cooldown to the effect of the Gospel skill on login, to prevent abuse. originsro/originsro#1693
- **Commands**: Extended the `@rates` command to include the quest experience rate. originsro/originsro#1632
- **Quests**: Removed any unrecoverable failure cases from The Sign quest. It's now possible to retry and continue the quest in every case that would normally result in permanent failure. Players that already reached adead end before this patch may request the staff's assistance to continue from the closest checkpoint.
- **Quests**: Improved time checks in The Sign quest. Instead of having a daily 2-hour time window to continue the quest, it'll be possible to continue at any time of the day as long as two hours have passed.
- **Commands**: Improved `@navshop` to highlight the selected shop's balloon when reaching the destination.
- **Skills**: Improved the description of the Ganbantein skill.
- **UI**: Removed the restriction on Etc items to be placed on the skill/shortcut bars. It's now possible to put any items in them, i.e. to keep track of the remaining Blue Gemstones.
- **UI**: Improved word-wrapping in the equipment window. Item names are now split into different lines on the word boundaries, where possible.
- **Skills**: Improved performance with the throwing animation used by certain skills, in WoE or `/mineffect` environments.
- **Skills**: Disabled the saving of the skill tree state to the Windows Registry, since it's not designed for multi-account multi-client servers. This considerably speeds up the loading time.
- **UI**: The search box in the Storage window is now case-insensitive, making it easier to find the wanted items.
- **Skills**: Improved performance of the Sharp-Shooting animation, in WoE or `/mineffect` environments.

#### Fixed

- **Misc**: Fixed some cases of Master Storage not working after going to the char selection screen and back to the game.
- **NPCs**: Fixed some warp portals not behaving like normal warp portals (for example, not ignoring cloaked characters).
- **Skills**: Fixed an excess of hitlock caused by Water Ball.
- **Skills**: Fixed Concentration not clearing the visual effect on termination.
- **NPCs**: Fixed the remaining time information of the Brave Warrior NPC during the last 24 hours of floating rates.
- **NPCs**: Increased the Brave Warrior single-donation limit from 10M to the fund limit (50M).
- **Skills**: Fixed the elemental damage rate reported by Sense in case of negative values.
- **Skills**: Fixed the status timer indicators showing an incorrect remaining time on relog.
- **UI**: Fixed a visual glitch causing the party name not to get correctly cleared when kicking members from a party.
- **Misc**: Performance and security improvements.
- **Skills**: Fixed an exploit with negative buffs when casting Marionette Control. The bonus can now never be higher than (99 - [base stat]), for each stat. Players that were unaware of the bug and already built their characters with the intent to exploit that game mechanic may receive assistance in the form of a partial reallocation. If you are affected, please open a ticket at https://gitlab.com/originsro/originsro-support showing your originally planned build and the build correction you wish to apply including (links to rocalc.com is recommended), subject to staff approval.
- **Mobs**: Fixed the PvP Gemini summons being unkillable.
- **Skills**: Fixed the Berserk skill not fully healing the caster in some cases. originsro/originsro#1669
- **Skills**: Fixed an exploitable bug when using Class Change on mobs affected by the Keeping, Stop or Barrier statuses.
- **Skills**: Fixed an issue that caused Basilica to be able to restrain MVPs.
- **Skills**: Fixed the Hiding skill not fully hiding falcons in certain cases.
- **Skills**: Fixed the appearance of the Taekwon and Super Novice level up angels.
- **Misc**: Removed an useless message "Nothing found in the selected map" message.
- **Mobs**: Fixed an issue that caused the ghost image of dead monsters to occasionally reappear.
- **Skills**: Fixed an issue that allowed casting a specific level of a skill with non-selectable levels, through a modified client.
- **Items**: Fixed visual glitches when wearing costumes in WoE.
- **Items**: Fixed the Twilight Scrolls not correctly consuming SP and Medicine Bowls.
- **UI**: Fixed the party member coordinates on the mini-map getting occasionally stuck. originsro/originsro#1240
- **Skills**: Fixed some incorrectly listed requirements for the Counter Instinct and Spiral Pierce skills.
- **Items**: Fixed the description of the Membership Card item. originsro/originsro#1706
- **Items**: Fixed the display sprites of Flamberge and Ring Pommel Saber.
- **Items**: Fixed the item description of Elven Ears, when using a custom skin tone.
- **UI**: Fixed the client loading screen never showing 100%, but stopping at 96%. Additionally, the loading screen is now up to 16% faster.
- **Skills**: Fixed the visual effect of the Blind status on widescreen resolutions.
- **Skills**: Fixed the visual effect of the Poison status on widescreen resolutions.
- **UI**: Fixed overlapping text in the Trade window.
- **Skills**: Fixed some invalid characters in the Romantic Rendezvous skill effect.
- **Misc**: Fixed the game screenshots showing up distorted and with incorrect colors, in some rare hardware and software configurations.
- **UI**: Fixed a rare client crash in the right-click menu.
- **Misc**: Fixed some cases that caused characters to seem to be facing the wrong direction.
- **Skills**: Fixed the Ice Wall skill creating dead, unwalkable cells in some cases.
- **Misc**: Fixed a possible client crash when receiving a chat message with a particular structure (not normally reproducible through chat messages sent by players).
- **Misc**: Fixed the damage numbers not displaying when hitting an enemy in particular cells of certain maps.
- **Skills**: Fixed the Potion Pitcher skill, now showing the correct animation for each level.
- **Commands**: The `/mineffect` state is now remembered when teleporting. originsro/originsro#1705
- **UI**: Fixed some cases that caused the mouse cursor to show the name and target a character different from the one hovered.

#### Removed

- **Skills**: Removed Lucifer's Lament from the Arrow Crafting ingredients, to avoid accidental use.
- **Events**: Removed the content related to the Halloween Event. The Chef in Niflheim will still be available throughout the duration of the Christmas Event.
- **Commands**: Removed the automatic activation of the `/help` command on login, unnecessarily spamming the chat window.
- **Chat**: Removed the protection against repeating multiple times the same chat message. It's now possible to repeat a `@`-command more than three times, without having to send pointless chat messages in `#main` just to reset the counter.

This update contains 404 commits.
