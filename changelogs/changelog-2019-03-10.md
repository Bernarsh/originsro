# Update Changelog - 2019-03-10 (part of Ep. 10.0)

#### OriginsRO Episode 10.0 Content

- **Maps**:     Added Lighthalzen and the Lighthalzen fields.
- **Maps**:     Added the Somatology Laboratory level 1 and 2.
- **Maps**:     Added the International Airship (Izlude - Juno).
- **Maps**:     Extended the Domestic Airship to include Lighthalzen (Juno - Einbroch - Lighthalzen).
- **Classes**:  Added the Homunculus system. Note: for game balance purposes, Lif's race has been changed to Plant.
- **Mobs**:     Added the monsters spawning in the newly introduced maps.
- **Items**:    Added the items related to the newly introduced maps.
- **Quests**:   Added various quests related to Lighthalzen:
  - Renown Detective's Cap Quest
  - Baby Pacifier Quest
  - Mystic Rose Quest
  - Red Bonnet Quest
  - How the Airship Works
  - Heart Fragment Quest
  - Friendship Quest
  - President Quest
  - Rekenber Job Quest
- **Quests**:   Added the Doomed Sword Quest.
- **NPCs**:     Added the Socket Enchanter NPCs (note: some items aren't available in the current episode).
- **Items**:    Added 10 new costumes:
  - Angel Wing Ears Costume
  - Stellar Costume
  - Mini Glasses Costume
  - Zorro Masque Costume
  - Ghost Bandana Costume
  - Diver Goggles Costume
  - Alarm Mask Costume
  - Decorative Golden Bell Costume
  - Machoman's Glasses Costume
  - Blue Beanie Costume

#### Added

- **Items**:    Added Spiritual Potion Creation (I, II, III) scrolls, in order to make it easier to brew common WoE consumables. The scrolls are available at the alchemist materials shop, for 20,000z.
- **WoE**:      Open two castles for each WoE session. The castles will be from two different realms, but each realm will have a different castle open each day.
- **NPCs**:     Integrated the alchemist material seller shop with additional potion creation related items.
- **NPCs**:     Added White Potions to the Izlude Tool Dealer.
- **NPCs**:     Added inns and repair shops to Amatsu, Ayothaya, Comodo, Izlude, Juno, Kunlun, Luoyang, Umbala.
- **NPCs**:     Added a limited teleport service (to Prontera only) to the Kafras in Amatsu, Ayothaya, Kunlun, Luoyang.
- **NPCs**:     Added a limited teleport service (to the nearest town only) to the Kafras in the Archer Village, Orc Dungeon, Coal Mine.
- **Misc**:     Added an option to manually choose the skin color to use for character creation in each game account. Part of OriginsRO#818
- **Commands**: Added new command aliases `@wi` (`@whereis`), `@wd` (`@whodrops`), `@wb` (`@whobuys`), `@mm` (`@mapmobs`).
- **AI**:       Added a custom User AI for homunculi, based on MirAI. It can be enabled with `/hoai`. Players that prefer a different AI, can replace it in the `USER_AI` folder.
- **Launcher**: Added a button to launch the MirAI configuration tool from the launcher, under the Settings tab (Mac version only, the Windows version will follow later).

#### Changed

- **NPCs**:     Streamlined the Kafra teleport system, now allowing teleport to all the available locations, from each town (the price of each warp is calculated as the sum of the prices of the individual warps that would have been necessary in the past).
- **NPCs**:     Changed some menu-based NPC shops to real shops (alchemist and blacksmith related material sellers).
- **Skills**:   Enabled use of the Super Novice Fury chant starting at 0% exp when at level 99.
- **NPCs**:     Improved usability of the Refine NPC:
  - The position of the manual input and safe limit options in the main menu have been inverted to make the most commonly used one easier to choose.
  - The manual input option now asks for the desired refine level rather than how many times.
  - More information is displayed during use, to make it harder to select the wrong item.
- **NPCs**:     Changed the Kafra Passes to a 1200z discount on each operation, rather than a 100% discount, for compatibility with the streamlined teleport system.
- **Skills**:   Reduced the minimum vertical distance between shops and chats from 2 to 1 cell.
- **Maps**:     Updated areas restricted from opening shops and chats:
  - Added areas with shop restrictions to Aldebaran, Amatsu, Juno, Kunlun, Lighthalzen, Luoyang, Morroc
  - Updated the shop-restricted areas in Payon and Izlude
  - Removed chat-restricted areas from Payon and Izlude.
- **Skills**:   Improved behavior of the Buying Store shops, now automatically closing when no longer able to buy items.
- **Launcher**: Improved detection of recent macOS versions in the generated reports (Mac version only).
- **Launcher**: Updaed third party libraries for better compatibility with recent OS versions (Mac version only).

#### Fixed

- **Items**:    Fixed an issue that caused some items not to be consumed correctly if used after casting Abracadabra.
- **NPCs**:     Fixed an issue that could allow unexpected script behavior by sending a forged packet through third party illegal tools.
- **NPCs**:     Fixed some NPC dialogue typos.
- **Skills**:   Fixed an issue that allowed casting the Monster Chant skill in town.
- **Items**:    Fixed an exploit that allowed to bypass the inactivity timeout by using items.
- **Mobs**:     Fixed an issue that would cause players to get stuck if they were interacting with a tombstone when its MVP respawned. OriginsRO#1096
- **Items**:    Fixed the name of the item Huuma Calm Mind Shuriken.

#### Removed

- **Channels**: Removed the #irc channel.
- **Misc**:     Removed the unintuitive Hometown system and its effects on the skin color when creating a character.

This update contains 328 commits.

# Update Changelog - 2019-03-22 (supplementary update)

#### Changed

- **Commands**:  Extended the `@hominfo` command to include the base level.
- **NPCs**:      Improved the logging of castle treasures, to reduce support time in case of server crash.
- **NPCs**:      Sorted the costume list alphabetically (for each price range) in the Costume Exchanger NPC.
- **Homunculi**: Reduced the maximum number of concurrently active homunculi (outside towns) to 5 per player.
- **Homunculi**: Increased the range of the Moonlight (Filir) skill to match its original pre-renewal value.
- **Maps**:      Enabled the use of `/memo` in Lighthalzen, in the areas where Teleport is allowed.
- **Maps**:      Added mini-map icons in Lighthalzen.

#### Fixed

- **Misc**:      Fixed an issue that caused the server to crash.
- **Items**:     Fixed an issue that could cause the idle timer to reset under certain conditions.
- **Homunculi**: Fixed an issue that caused the Homunculus HP/SP bars to refill on relog. OriginsRO#1287
- **Client**:    Fixed a client visual glitch on login, under certain conditions.
- **Items**:     Fixed the description of Zealotus Egg.
- **Skills**:    Fixed the range of Water Ball level 6~10 (Plagiarize).

This update contains 18 commits.
