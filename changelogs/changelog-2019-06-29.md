# Update Changelog - 2019-06-29 (part of Ep. 10.0)

#### Changed

- **Mobs**:   Changed the Mob of the Day criteria, to exclude monsters with very small spawn amounts on a map (increased from 10 to 16).
- **NPCs**:   Tweaked the position of various NPCs in the area in front of the Clock Tower in Aldebaran.
- **Skills**: Changed the Mental Charge skill not to terminate when teleporting or changing maps.
- **Mobs**:   Reverted the Swordsman Egnigem Cenia monster name to its gender-neutral version.
- **WoE**:    Changed the WoE castle rotation time from 04:00 to 12:00, giving guild leaders 12 hours to claim the treasure chests on the rotation day, instead of just 4.
- **Skills**: Added a message bubble when casting Item Appraisal, to be consistent with other skills.
- **NPCs**:   Changed the order of the guild castle Kafra's menu to match the ones in town.
- **Skills**: Disabled the Taekwon Mission skill in certain maps where it may be abusable.
- **Skills**: Added a restriction to prevent Buying Store listings with a price lower than the NPC sell price.
- **Skills**: Changed the automatically targeted skills not to reset the idle timer, to avoid exploits. (NOTE: this will be reverted and replaced with a less strict version in the next update.)
- **Misc**:   Fixed an exploit that could make it possible to reset the idle timer through the use of a client illegally modified.

#### Fixed

- **Skills**: Fixed the Gunslinger skill Bull's Eye not treating players as demi-humans. OriginsRO#1306
- **WoE**:    Fixed an issue in the castle rotation system. (note: this had already been applied to the server through a hotfix).
- **Skills**: Fixed some problems in the interaction between the Cloaking skill and certain warps.
- **Client**: Fixed an issue in the character selection, not refreshing correctly after deleting a character.
- **Misc**:   Fixed an issue that could cause memory corruption and server crashes.
- **Homunculi**: Fixed an issue that could keep homunculi from automatically Resting when entering a WoE map if they were below 80% of their HP.
- **NPCs**:   Fixed the Tailor NPC potentially offering money as refund for clothes that were purchased through a coupon.
- **NPCs**:   Fixed the time display format for the floating rates cooldown and duration.
- **Skills**: Fixed the Arrow Craft item list, skipping some items if there are unidentified items in the inventory.
- **Quests**: Fixed some errors in the How the Airship Works quest.
- **NPCs**:   Fixed a visual glitch in the International Airship announcements, mentioning it was heading to Rael even though it's not available.
- **Skills**: Fixed the song/dance buffs being refreshed with the wrong stats upon entering a song from a different performer. OriginsRO#1432
- **CP**:     Fixed a glitch in the Market page not removing certain entries as they're removed from a Buying Store.
- **Skills**: Fixed the Magic Rod's animation when triggered by Spellbreaker.
- **Misc**:   Fixed an issue that could cause Experience loss during a routine check performed by a GM, in rare circumstances.
- **Misc**:   Fixed an issue that could cause the impossibility to log in from a different location into a character that was left at the character selection screen.
- **Skills**: Fixed an issue when more than two songs or dances overlap and cause the Dissonance or Hip Shaker effect.
- **NPCs**:   Changed the Mob of the Day NPC so that it is no longer seen as a monster by the Homunculus AI.
- **Skills**: Fixed the guild area skills to work correctly when used near a wall.

This update contains 189 commits.
